""" settings file """
from __future__ import annotations

import os
from pathlib import Path

from pydantic.v1 import BaseSettings


class Settings(BaseSettings):
    # app settings
    app_host: str
    app_port: int
    # db settings
    DATABASE_URL: str
    DB_HOST: str
    DB_PORT: int
    DB_PASS: str
    DB_NAME: str
    DB_USER: str
    # auth
    SECRET_KEY: str
    TOKEN_EXPIRE_MINUTES: int = 30
    ALGORITHM: str = "HS256"
    # TEST db settings
    TEST_DATABASE_URL: str
    TEST_DB_HOST: str
    TEST_DB_PORT: int
    TEST_DB_PASS: str
    TEST_DB_NAME: str
    TEST_DB_USER: str


settings = Settings(
    # _env_file=os.path.expanduser(".env"),
    _env_file=os.path.expanduser("example.env"),
    _env_file_encoding="utf-8",
)
