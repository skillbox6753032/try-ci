import pytest
from httpx import AsyncClient

from prjct.tests.conftest import base_url, prepare_db_with_1_of_each_entity


@pytest.mark.parametrize(
    "route",
    [
        f"{base_url}users/",
        f"{base_url}coffee/",
        f"{base_url}coffee/?incoming_coffee_id=1/",
        f"{base_url}",
    ],
)
async def test_get_routes_status(client: AsyncClient, route: str):
    if route == f"{base_url}coffee/?incoming_coffee_id=1/":
        await prepare_db_with_1_of_each_entity()
    rv = await client.get(route)
    assert rv.status_code == 200


async def test_sign_up(client: AsyncClient):
    await prepare_db_with_1_of_each_entity()
    user_data = {
        "name": "test-reg",
        "surname": "test-reg",
        "email": "test-reg@example.com",
        "address": {
            "city": "test-reg",
            "street_name": "test-reg",
            "street_address": "test-reg",
            "zip_code": "test-reg",
            "state": "test-reg",
            "country": "test-reg",
            "coordinates": {"lat": 1.23, "lng": 4.56},
        },
        "coffee_id": 1,
        "password": "reg",
    }
    res = await client.post(f"{base_url}auth/sign-up", json=user_data)

    assert res.status_code == 200
    res_json = res.json()
    assert "access_token" in res_json
    assert "token_type" in res_json
    assert res_json["token_type"] == "Bearer"


async def test_sign_in(client: AsyncClient):
    await prepare_db_with_1_of_each_entity()
    test_creds = {
        "username": "test@example.com",
        "password": "test",
    }
    res = await client.post(f"{base_url}auth/sign-in", data=test_creds)
    assert res.status_code == 200
    res_json = res.json()
    assert "access_token" in res_json
    assert "token_type" in res_json
    assert res_json["token_type"] == "Bearer"
