import asyncio
from typing import AsyncGenerator, AsyncIterator

import pytest
import pytest_asyncio
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from prjct.db import db_models
from prjct.db.db_models import Base
from prjct.main import app
from prjct.utils.hash_utils import hash_password
from prjct.utils.unit_of_work import UnitOfWork
from settings import settings

base_url = "http://127.0.0.1:8000/"

test_engine = create_async_engine(
    settings.TEST_DATABASE_URL,
    future=True,
)

test_async_session_maker = sessionmaker(
    bind=test_engine,
    expire_on_commit=False,
    class_=AsyncSession,
)


class TestUnitOfWork(UnitOfWork):
    def __init__(self):
        super().__init__()
        self.session_maker = test_async_session_maker


@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


async def prepare_db() -> None:
    async with test_engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)


@pytest_asyncio.fixture(scope="session")
async def client() -> AsyncGenerator[AsyncClient, None]:
    app.dependency_overrides.update({UnitOfWork: TestUnitOfWork})
    async with AsyncClient(app=app) as c:
        yield c


async def get_session_from_test_db() -> AsyncIterator[AsyncSession]:
    async with test_async_session_maker() as session:
        yield session


async def prepare_db_with_1_of_each_entity() -> None:
    await prepare_db()
    coffee_data = {
        "title": "test",
        "origin": "test",
        "intensifier": "test",
        "notes": [
            "test 1",
            "test 2",
        ],
    }
    user_data = {
        "name": "test",
        "surname": "test",
        "email": "test@example.com",
        "address": {
            "city": "test",
            "street_name": "test",
            "street_address": "test",
            "zip_code": "test",
            "state": "test",
            "country": "test",
            "coordinates": {"lat": 1.23, "lng": 4.56},
        },
        "coffee_id": 1,
        "password": "test",
    }

    session_iterator = get_session_from_test_db()
    session = await anext(session_iterator)
    new_coffee = db_models.Coffee(
        title=coffee_data["title"],
        origin=coffee_data["origin"],
        intensifier=coffee_data["intensifier"],
        notes=coffee_data["notes"],
    )
    session.add(new_coffee)
    await session.commit()

    new_user = db_models.User(
        name=user_data["name"],
        surname=user_data["surname"],
        email=user_data["email"],
        hashed_psw=hash_password(user_data["password"]),
        address=user_data["address"],
        coffee_id=1,
    )
    session.add(new_user)
    await session.commit()
    await session.close()
