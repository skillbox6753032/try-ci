from httpx import AsyncClient

from prjct.tests.conftest import base_url, prepare_db_with_1_of_each_entity


async def test_create_coffee(client: AsyncClient):
    await prepare_db_with_1_of_each_entity()
    coffee_data = {
        "title": "test new",
        "origin": "test new",
        "intensifier": "test new",
        "notes": [
            "test 1 new",
            "test 2 new",
        ],
    }
    res = await client.post(f"{base_url}coffee/", json=coffee_data)

    assert res.status_code == 200
    result_dict = res.json()
    assert result_dict == {
        "title": "test new",
        "origin": "test new",
        "intensifier": "test new",
        "notes": [
            "test 1 new",
            "test 2 new",
        ],
        "id": 2,
    }
