from httpx import AsyncClient

from prjct.tests.conftest import base_url, prepare_db_with_1_of_each_entity


async def test_create_user(client: AsyncClient):
    await prepare_db_with_1_of_each_entity()
    user_data = {
        "name": "test-new",
        "surname": "test-new",
        "email": "test-new@example.com",
        "address": {
            "city": "test-new",
            "street_name": "test-new",
            "street_address": "test-new",
            "zip_code": "test-new",
            "state": "test-new",
            "country": "test-new",
            "coordinates": {"lat": 1.23, "lng": 4.56},
        },
        "coffee_id": 1,
        "password": "new",
    }
    res = await client.post(f"{base_url}users/", json=user_data)

    assert res.status_code == 200
    result_dict = res.json()
    del result_dict["user_id"]
    assert result_dict == {
        "name": "test-new",
        "surname": "test-new",
        "email": "test-new@example.com",
        "address": {
            "city": "test-new",
            "street_name": "test-new",
            "street_address": "test-new",
            "zip_code": "test-new",
            "state": "test-new",
            "country": "test-new",
            "coordinates": {"lat": 1.23, "lng": 4.56},
        },
        "coffee_id": 1,
        "is_active": True,
    }
