from prjct.db import db_models
from prjct.utils.repositories import SQLAlchemyRepository


class CoffeeRepository(SQLAlchemyRepository):
    model = db_models.Coffee
