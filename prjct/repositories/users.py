from uuid import UUID

from fastapi import HTTPException
from sqlalchemy import Unicode
from sqlalchemy.exc import MultipleResultsFound, NoResultFound
from sqlalchemy.future import select
from starlette import status

from prjct.db import db_models
from prjct.schemas.users import UserUpdate
from prjct.utils.repositories import SQLAlchemyRepository


class UsersRepository(SQLAlchemyRepository):
    model = db_models.User

    async def _get(self, user_id: UUID):
        res = await self.session.execute(
            select(self.model).where(self.model.user_id == user_id),
        )
        return res.scalars().one()

    async def get(self, user_id: UUID):
        return await self._get(user_id)

    async def update(self, some_id: UUID, some_data: UserUpdate):
        update_item = await self._get(some_id)

        for field, value in some_data:
            if value and field.lower() != "address":
                setattr(update_item, field, value)

                # updating address field if exists
            elif value and field.lower() == "address":
                setattr(update_item, field, value.model_dump())
        self.session.add(update_item)
        return update_item

    async def get_users_by_country(self, some_country: str) -> list[db_models.User]:
        res = await self.session.execute(
            select(self.model).filter(
                db_models.User.address["country"]
                .astext.cast(
                    Unicode,
                )
                .like(f"%{some_country}%"),
            ),
        )
        return [user for user in res.scalars().all()]

    async def get_user_by_email_for_auth(
        self, user_email: str
    ) -> db_models.User | None:
        res = await self.session.execute(
            select(self.model).where(self.model.email == user_email),
        )
        try:
            user = res.scalars().one()
        except (NoResultFound, MultipleResultsFound):
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Email <{user_email}> is not found",
            )
        return user
