from __future__ import annotations

import uvicorn
from fastapi import FastAPI

from prjct.api import router
from prjct.utils.load_test_data import delete_test_data, load_test_data
from settings import settings

app = FastAPI()
app.include_router(router)


@app.on_event("startup")
async def load_mock_data():
    await delete_test_data()
    await load_test_data()
    pass


@app.on_event("shutdown")
async def the_end():
    await delete_test_data()
    pass


if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host=settings.app_host,
        port=settings.app_port,
        reload=True,
        log_level="debug",
    )
