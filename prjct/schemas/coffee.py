from __future__ import annotations

from pydantic import BaseModel


class CoffeeBase(BaseModel):
    title: str
    origin: str
    intensifier: str
    notes: list[str]


class Coffee(CoffeeBase):
    id: int

    class Config:
        from_attributes = True


class CoffeeCreate(CoffeeBase):
    pass


class CoffeeUpdate(BaseModel):
    title: str | None = None
    origin: str | None = None
    intensifier: str | None = None
    notes: list[str] | None = None
