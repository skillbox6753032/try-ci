from __future__ import annotations

import re
import uuid

from fastapi import HTTPException
from pydantic import BaseModel, EmailStr
from pydantic.v1 import validator

ONLY_LETTERS_PATTERN = re.compile(r"^[а-яА-Яa-zA-Z\-]+$")


class Coordinates(BaseModel):
    lat: float
    lng: float


class Address(BaseModel):
    city: str
    street_name: str
    street_address: str
    zip_code: str
    state: str
    country: str
    coordinates: Coordinates


class TunedModel(BaseModel):
    class Config:
        """tells pydantic to convert even non dict obj to json"""

        from_attributes = True


class BaseUser(BaseModel):
    name: str
    surname: str
    email: EmailStr
    address: Address
    coffee_id: int


class User(BaseUser, TunedModel):
    user_id: uuid.UUID
    is_active: bool


class UserCreate(BaseUser):
    password: str

    @validator("name")
    @classmethod
    def validate_name(cls, value):
        if not ONLY_LETTERS_PATTERN.match(value):
            raise HTTPException(
                status_code=422,
                detail="Name field should contains only letters",
            )
        return value

    @validator("surname")
    @classmethod
    def validate_surname(cls, value):
        if not ONLY_LETTERS_PATTERN.match(value):
            raise HTTPException(
                status_code=422,
                detail="Surname field should contains only letters",
            )
        return value


class UserUpdate(BaseModel):
    name: str | None = None
    surname: str | None = None
    email: EmailStr | None = None
    password: str | None = None
    is_active: bool | None = None
    address: Address | None = None
