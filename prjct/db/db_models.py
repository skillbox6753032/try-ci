from __future__ import annotations

import uuid
from typing import Type

import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ARRAY, JSON, TEXT, UUID
from sqlalchemy.orm import DeclarativeMeta, declarative_base, relationship

from prjct.schemas.users import User as UserSchema


def get_base() -> Type[DeclarativeMeta]:
    return declarative_base()


Base: Type[DeclarativeMeta] = get_base()


class User(Base):
    __tablename__ = "users"

    user_id = sa.Column(
        UUID(as_uuid=True),
        primary_key=True,
        default=uuid.uuid4,
    )
    name = sa.Column(sa.String, nullable=False)
    surname = sa.Column(sa.String, nullable=False)
    email = sa.Column(sa.String, nullable=False, unique=True)
    is_active = sa.Column(sa.Boolean, default=True)
    hashed_psw = sa.Column(sa.String, nullable=False)
    address = sa.Column(JSON)
    coffee_id = sa.Column(
        sa.Integer,
        sa.ForeignKey(
            "coffee.id",
        ),
        nullable=True,
        default=None,
    )

    def read_model(self) -> UserSchema:
        return User(
            user_id=self.user_id,
            name=self.name,
            surname=self.surname,
            email=self.email,
            is_active=self.is_active,
            hashed_psw=self.hashed_psw,
            address=self.address,
            coffee_id=self.coffee_id,
        )


class Coffee(Base):
    __tablename__ = "coffee"
    id = sa.Column(sa.Integer, primary_key=True)
    title = sa.Column(sa.String, nullable=False, unique=True)
    origin = sa.Column(sa.String, nullable=False)
    intensifier = sa.Column(sa.String, nullable=False)
    notes = sa.Column(ARRAY(TEXT))
    users = relationship("User", backref="coffee")

    def read_model(self) -> UserSchema:
        return User(
            id=self.id,
            title=self.title,
            origin=self.origin,
            intensifier=self.intensifier,
            notes=self.notes,
            users=self.users,
        )
