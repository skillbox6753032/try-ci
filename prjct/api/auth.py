from __future__ import annotations

from fastapi import APIRouter, Depends
from fastapi.security.oauth2 import OAuth2PasswordRequestForm

from prjct.schemas.auth import Token
from prjct.schemas.users import User, UserCreate
from prjct.services.auth import AuthService, get_current_user
from prjct.utils.dependencies import uow_dependency

router = APIRouter(
    prefix="/auth",
    tags=["auth"],
)


@router.post("/sign-up", response_model=Token)
async def sign_up(
    user_data: UserCreate,
    uow: uow_dependency,
) -> Token:
    """registration endpoint"""
    return await AuthService().register_new_user(uow, user_data)


@router.post("/sign-in", response_model=Token)
async def sign_in(
    uow: uow_dependency,
    form_data: OAuth2PasswordRequestForm = Depends(),
) -> Token:
    """authentication endpoint"""
    return await AuthService().authenticate_user(
        uow=uow,
        email=form_data.username,
        password=form_data.password,
    )


@router.get("/current_user", response_model=User)
async def current_user(user: User = Depends(get_current_user)) -> User:
    """get current user for testing purpose"""
    return user
