from __future__ import annotations

from typing import Sequence

from fastapi import APIRouter
from starlette import status
from starlette.responses import Response

from prjct.schemas.coffee import Coffee, CoffeeCreate, CoffeeUpdate
from prjct.services.coffee import CoffeeService
from prjct.utils.dependencies import uow_dependency

router = APIRouter(
    prefix="/coffee",
    tags=["coffee"],
)


@router.get("/", response_model=Sequence[Coffee])
async def read_all_coffees_route(
    uow: uow_dependency,
) -> Sequence[Coffee]:
    """endpoint for reading all coffee"""
    return await CoffeeService.get_list(uow)


@router.get("/<incoming_coffee_id>", response_model=Coffee)
async def read_coffee_by_id_route(
    incoming_coffee_id: int,
    uow: uow_dependency,
) -> Coffee:
    """endpoint for reading one coffee data by id"""
    return await CoffeeService.get(uow, incoming_coffee_id)


@router.post("/", response_model=Coffee)
async def crate_coffee_route(
    coffee_data: CoffeeCreate,
    uow: uow_dependency,
) -> Coffee:
    """endpoint for creating new coffee"""
    return await CoffeeService.create(uow, coffee_data)


@router.delete("/<incoming_coffee_id>")
async def delete_coffee_route(
    incoming_coffee_id: int,
    uow: uow_dependency,
) -> Response:
    """endpoint for deleting particular user data by user id"""
    await CoffeeService.delete(uow, incoming_coffee_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.patch("/<incoming_coffee_id>", response_model=Coffee)
async def update_coffee_route(
    incoming_coffee_id: int,
    incoming_coffee_data: CoffeeUpdate,
    uow: uow_dependency,
) -> Coffee:
    return await CoffeeService.update(uow, incoming_coffee_id, incoming_coffee_data)
