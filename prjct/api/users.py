from __future__ import annotations

from typing import Sequence
from uuid import UUID

from fastapi import APIRouter
from starlette import status
from starlette.responses import Response

from prjct.schemas.users import User, UserCreate, UserUpdate
from prjct.services.users import UsersService
from prjct.utils.dependencies import uow_dependency

router = APIRouter(
    prefix="/users",
    tags=["users"],
)


@router.get("/", response_model=Sequence[User])
async def read_all_users_route(
    uow: uow_dependency,
) -> Sequence[User]:
    """endpoint for reading all users"""
    return await UsersService.get_list(uow)


@router.get("/<user_data>", response_model=User)
async def read_user_by_id_route(
    user_data: UUID,
    uow: uow_dependency,
) -> User:
    """endpoint for reading one user data by user_id"""
    return await UsersService.get(uow, user_data)


@router.post("/", response_model=User)
async def crate_user_route(
    user_data: UserCreate,
    uow: uow_dependency,
) -> User:
    """endpoint for creating new user"""
    return await UsersService.create(uow, user_data)


@router.delete("/")
async def delete_user_route(
    user_data: UUID,
    uow: uow_dependency,
) -> Response:
    """endpoint for deleting particular user data by user id"""
    await UsersService.delete(uow, user_data)
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.patch("/<incoming_user_id>", response_model=User)
async def update_user_route(
    incoming_user_id: UUID,
    incoming_user_data: UserUpdate,
    uow: uow_dependency,
) -> User:
    return await UsersService.update(uow, incoming_user_id, incoming_user_data)
