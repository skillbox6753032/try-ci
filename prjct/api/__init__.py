from __future__ import annotations

from fastapi import APIRouter

from prjct.api.auth import router as auth_router
from prjct.api.coffee import router as coffee_router
from prjct.api.ping import router as ping_router
from prjct.api.users import router as users_router

router = APIRouter()

router.include_router(ping_router)
router.include_router(users_router)
router.include_router(auth_router)
router.include_router(coffee_router)
