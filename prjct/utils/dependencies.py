from typing import Annotated

from fastapi import Depends

from prjct.utils.unit_of_work import IUnitOfWork, UnitOfWork

uow_dependency = Annotated[IUnitOfWork, Depends(UnitOfWork)]
