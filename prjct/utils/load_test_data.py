from __future__ import annotations

import json
import random
from pathlib import Path

import aiofiles
from sqlalchemy.future import select

from prjct.db import db_models
from prjct.db.db_conn import get_session
from prjct.utils.hash_utils import hash_password


async def read_file(some_path: Path) -> list[dict[str, str]]:
    async with aiofiles.open(some_path, mode="rb") as file:
        return json.loads(await file.read())


async def load_test_data():
    # files paths
    users_json_path = Path.cwd() / "prjct" / "mock_users.json"
    coffees_json_path = Path.cwd() / "prjct" / "mock_coffee.json"

    # read files
    users_data_list = await read_file(users_json_path)
    coffee_data_list = await read_file(coffees_json_path)

    session_gen = get_session()
    session = await anext(session_gen)

    # handle coffees

    new_coffees_list = []
    for coffee_data in coffee_data_list:
        new_coffee = db_models.Coffee(
            title=coffee_data["blend_name"],
            origin=coffee_data["origin"],
            intensifier=coffee_data["intensifier"],
            notes=coffee_data["notes"].split(", "),
        )
        new_coffees_list.append(new_coffee)
    session.add_all(new_coffees_list)
    await session.commit()

    # handle users

    coffees_ids_query = await session.execute(select(db_models.Coffee.id))
    coffees_ids = coffees_ids_query.scalars().all()

    new_users_list = []
    for user_data in users_data_list:
        new_user = db_models.User(
            name=user_data["first_name"],
            surname=user_data["last_name"],
            email=user_data["email"],
            hashed_psw=hash_password(user_data["password"]),
            address=user_data["address"],
            coffee_id=random.choice(coffees_ids),
        )
        new_users_list.append(new_user)
    session.add_all(new_users_list)
    await session.commit()


async def delete_test_data():
    session_gen = get_session()
    session = await anext(session_gen)

    delete_users = db_models.User.__table__.delete()
    delete_coffees = db_models.Coffee.__table__.delete()
    await session.execute(delete_users)
    await session.execute(delete_coffees)
    await session.commit()
