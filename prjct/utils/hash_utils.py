from __future__ import annotations

from passlib.hash import bcrypt


def verify_password(plain_pwd: str, hashed_pwd: str) -> bool:
    return bcrypt.verify(plain_pwd, hashed_pwd)


def hash_password(password: str) -> str:
    return bcrypt.hash(password)
