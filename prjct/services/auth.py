from __future__ import annotations

import datetime
import json

from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from pydantic import ValidationError
from sqlalchemy.exc import IntegrityError
from starlette import status

from prjct.db import db_models
from prjct.schemas.auth import Token
from prjct.schemas.users import User, UserCreate
from prjct.utils.hash_utils import hash_password, verify_password
from prjct.utils.unit_of_work import IUnitOfWork
from settings import settings

oauth2_schema = OAuth2PasswordBearer(tokenUrl="/auth/sign-in")


def get_current_user(token: str = Depends(oauth2_schema)) -> User:
    return AuthService.validate_token(token)


class AuthService:
    @classmethod
    def validate_token(cls, incoming_token: str) -> User:
        our_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={
                "WWW-Authenticate": "Bearer",
            },
        )

        try:
            payload = jwt.decode(
                incoming_token,
                settings.SECRET_KEY,
                algorithms=settings.ALGORITHM,
            )
        except JWTError:
            raise our_exception from None

        # get serialized user from decoded token
        user_data = payload.get("user")

        # validate user's data with pydantic
        try:
            user = User.model_validate(user_data)
        except ValidationError:
            raise our_exception from None

        return user

    @classmethod
    def create_token(cls, user: db_models.User) -> Token:
        user_data = User.model_validate(user)
        now = datetime.datetime.utcnow()

        payload = {
            "exp": (now + datetime.timedelta(minutes=settings.TOKEN_EXPIRE_MINUTES)),
            "sub": str(user_data.user_id),
            "user": json.loads(user_data.model_dump_json()),
        }

        token = jwt.encode(
            payload,
            settings.SECRET_KEY,
            algorithm=settings.ALGORITHM,
        )
        return Token(access_token=token, token_type="Bearer")

    async def register_new_user(self, uow: IUnitOfWork, user_data: UserCreate) -> Token:
        async with uow:
            try:
                new_user_dict = user_data.model_dump()
                new_user_dict["hashed_psw"] = hash_password(user_data.password)
                del new_user_dict["password"]

                new_user = await uow.users_repo.create(new_user_dict)
                await uow.commit()

                return self.create_token(new_user)

            except IntegrityError as e:
                if "UniqueViolationError" in str(e):
                    raise HTTPException(
                        status_code=status.HTTP_409_CONFLICT,
                        detail="User with such data already exists",
                    )
                if "ForeignKeyViolationError" in str(e):
                    raise HTTPException(
                        status_code=status.HTTP_409_CONFLICT,
                        detail="Incorrect coffee id",
                    )

    async def authenticate_user(
        self, uow: IUnitOfWork, email: str, password: str
    ) -> Token:
        our_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={
                "WWW-Authenticate": "Bearer",
            },
        )
        async with uow:
            user = await uow.users_repo.get_user_by_email_for_auth(user_email=email)

        if not user:
            raise our_exception

        if not verify_password(password, user.hashed_psw):
            raise our_exception

        return self.create_token(user)
