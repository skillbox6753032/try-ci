from __future__ import annotations

from fastapi import HTTPException
from sqlalchemy.exc import IntegrityError, NoResultFound, SQLAlchemyError
from starlette import status

from prjct.db import db_models
from prjct.schemas.coffee import Coffee, CoffeeCreate, CoffeeUpdate
from prjct.utils.unit_of_work import IUnitOfWork


class CoffeeService:
    @staticmethod
    async def create(
        uow: IUnitOfWork,
        coffee_data: CoffeeCreate,
    ) -> db_models.Coffee:
        async with uow:
            try:
                new_coffee = await uow.coffee_repo.create(coffee_data.model_dump())
                await uow.commit()
                return new_coffee

            except IntegrityError:
                raise HTTPException(
                    status_code=status.HTTP_409_CONFLICT,
                    detail="Coffee with such data already exists",
                )

    @staticmethod
    async def get_list(uow: IUnitOfWork) -> list[db_models.Coffee]:
        async with uow:
            try:
                return await uow.coffee_repo.get_list()
            except SQLAlchemyError:
                return []

    @staticmethod
    async def get(uow: IUnitOfWork, incoming_coffee_id: int) -> Coffee:
        async with uow:
            try:
                return await uow.coffee_repo.get(incoming_coffee_id)

            except NoResultFound:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail=f"Coffee with id=< {incoming_coffee_id} > is not found",
                )
            except Exception as e:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail=str(e),
                )

    @staticmethod
    async def delete(uow: IUnitOfWork, incoming_coffee_id: int) -> None:
        async with uow:
            try:
                await uow.coffee_repo.delete(incoming_coffee_id)
                await uow.commit()

            except NoResultFound:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
            except SQLAlchemyError:
                raise HTTPException(
                    status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                    detail="Problem with DB. Retry.",
                )

    @staticmethod
    async def update(
        uow: IUnitOfWork,
        incoming_coffee_id: int,
        incoming_coffee_data: CoffeeUpdate,
    ) -> db_models.Coffee:
        async with uow:
            try:
                updated_coffee = await uow.coffee_repo.update(
                    incoming_coffee_id, incoming_coffee_data
                )
                await uow.commit()
                return updated_coffee

            except NoResultFound:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
            except SQLAlchemyError:
                raise HTTPException(
                    status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                    detail="Problem with DB. Retry.",
                )
