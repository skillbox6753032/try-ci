from __future__ import annotations

from uuid import UUID

from fastapi import HTTPException
from sqlalchemy.exc import IntegrityError, NoResultFound, SQLAlchemyError
from starlette import status

from prjct.db import db_models
from prjct.schemas.users import User, UserCreate, UserUpdate
from prjct.utils.hash_utils import hash_password
from prjct.utils.unit_of_work import IUnitOfWork


class UsersService:
    @staticmethod
    async def create(
        uow: IUnitOfWork,
        some_user: UserCreate,
    ) -> db_models.User:
        async with uow:
            try:
                new_user_dict = some_user.model_dump()
                new_user_dict["hashed_psw"] = hash_password(some_user.password)
                del new_user_dict["password"]

                new_user = await uow.users_repo.create(new_user_dict)
                await uow.commit()
                return new_user

            except IntegrityError as e:
                if "UniqueViolationError" in str(e):
                    raise HTTPException(
                        status_code=status.HTTP_409_CONFLICT,
                        detail="User with such data already exists",
                    )
                if "ForeignKeyViolationError" in str(e):
                    raise HTTPException(
                        status_code=status.HTTP_409_CONFLICT,
                        detail="Incorrect coffee id",
                    )

    @staticmethod
    async def get_list(uow: IUnitOfWork) -> list[db_models.User]:
        async with uow:
            try:
                return await uow.users_repo.get_list()
            except SQLAlchemyError:
                return []

    @staticmethod
    async def get(uow: IUnitOfWork, user_data: UUID) -> User:
        async with uow:
            try:
                return await uow.users_repo.get(user_data)
            except NoResultFound:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail=f"User with id=< {user_data} > is not found",
                )

    @staticmethod
    async def delete(uow: IUnitOfWork, user_data: UUID) -> None:
        async with uow:
            try:
                await uow.users_repo.delete(user_data)
                await uow.commit()

            except NoResultFound:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
            except SQLAlchemyError:
                raise HTTPException(
                    status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                    detail="Problem with DB. Retry.",
                )

    @staticmethod
    async def update(
        uow: IUnitOfWork,
        incoming_user_id: UUID,
        incoming_user_data: UserUpdate,
    ) -> db_models.User:
        async with uow:
            try:
                updated_user = await uow.users_repo.update(
                    incoming_user_id, incoming_user_data
                )
                await uow.commit()
                return updated_user

            except NoResultFound:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
            except SQLAlchemyError:
                raise HTTPException(
                    status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                    detail="Problem with DB. Retry.",
                )
